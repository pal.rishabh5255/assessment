export default function iteratee(value, key, obj) {
    
    if (typeof value === 'string') {
        return value.toUpperCase();
    } else {
        return value*2;
    }
}

