import isObject from "../isObject.js";
import has from "../has.js";

export default function keys(obj) {
    // Checking obj is of type object or  not
    if (!isObject(obj)) 
    return [];
    // Directly accessing the properties of obkect
    if (Object.keys) 
    return Object.keys(obj);
    // Manually getting the properties of object if the above method doesn't work
    var keysArray = [];
    for (var key in obj) {
        if (has(obj, key)) keysArray.push(key);
    }
    return keysArray;
}
