import keys from "./keys.js";
export default function pairs(obj) {
    if (obj === null || typeof obj !== 'object') return [];
    const _keys = keys(obj);
    const length = _keys.length;
    const result = Array(length);
    for (let i = 0; i < length; i++) {
        result[i] = [_keys, obj[keys[i]]];
    }
    return result;
}
