import keys from "./keys.js";

export default function invert(obj) {
    if (obj === null) return {}; 
    var result = {};
    var _keys = keys(obj); 
    for (var key of _keys) {
      result[obj[key]] = key;
    }
    
    return result;
  }
  
  
