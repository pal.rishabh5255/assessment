export default function defaults(obj, defaultProps) {
    for (const key in defaultProps) {
        if (Object.prototype.hasOwnProperty.call(defaultProps, key)) {
            // Check if the property is not present in obj or if it's undefined
            if (!(key in obj) || obj[key] === undefined) {
                obj[key] = defaultProps[key];
            }
        }
    }
    return obj;
}
